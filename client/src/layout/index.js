import React, { useEffect, useState } from 'react';
import { Navbar, Container, Nav, NavDropdown, Card, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../assets/css/layout.css';
import userData from '../dummy-user.json';
import LoginForm from '../pages/Login';

const Layout = (props) => {
    const [user, setUser] = useState({username: '', password: ''});
    const [error, setError] = useState('');

    return (
        <div className="layout">
            <Navbar className='navbar' bg="dark" expand="lg">
                <Container fluid>
                    <Navbar.Brand href="home">SUIT GAME</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto">
                        {
                            (user.username !== '' && user.password !== '') ? (
                                <>
                                    
                                </>                                
                            ):(
                                <>
                                    <Nav.Link as={Link} to="/register">REGISTER</Nav.Link>
                                    <Nav.Link as={Link} to="/login">LOGIN</Nav.Link>
                                </>
                            )
                        }
                        
                    </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>

            {props.children}
            
            <footer>
                <Container fluid>
                    <Row className='justify-content-md-center text-center'>
                        <Col md="1">
                            <Link as={Link} to="#">MAIN</Link>
                        </Col>
                        <Col md="1">
                            <Link as={Link} to="#">ABOUT</Link>
                        </Col>
                        <Col md="auto">
                            <Link as={Link} to="#">GAME FEATURES</Link>
                        </Col>
                        <Col md="auto">
                            <Link as={Link} to="#">SYSTEM REQUIREMENT</Link>
                        </Col>
                        <Col md="1">
                            <Link as={Link} to="#">QUOTES</Link>
                        </Col>
                    </Row>

                    <hr></hr>

                    <Row className='no-gutters justify-content-md-center'>
                        <Col md="4">
                            <p>© 2021 Your Games, Inc. All Rights Reserved</p>
                        </Col>
                        <Col md="4" className='d-flex justify-content-md-end'>
                            <p>Privacy Policy | Terms of Services | Code of Conduct</p>
                        </Col>
                    </Row>
                </Container>
            </footer>

            {/* <Navbar className='footer' bg="dark" expand="lg">
                <Container>
                    <Nav className="d-flex justify-content-center">
                        <Nav.Link as={Link} to="#">MAIN</Nav.Link>
                        <Nav.Link as={Link} to="#">ABOUT</Nav.Link>
                        <Nav.Link as={Link} to="#">GAME FEATURES</Nav.Link>
                        <Nav.Link as={Link} to="#">SYSTEM REQUIREMENT</Nav.Link>
                        <Nav.Link as={Link} to="#">QUOTES</Nav.Link>
                    </Nav>
                </Container>
            </Navbar> */}
        </div>
    );    
}

export { Layout };