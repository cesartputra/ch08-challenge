import React, { useEffect, useState } from "react";
import { Layout } from '../layout';
import { Container, Row, Col, Card, Button, Form } from 'react-bootstrap';
import '../assets/css/auth.css';
import userData from '../dummy-user.json';

const Login = ({ Login, error }) => {
    const [details, setDetails] = useState({username: '', password: ''})

    const handleLogin = e => {
        e.preventDefault();

        Login(details);
    }

    return (
        <Layout>            
            <Container fluid>
                <Row className="loginPage justify-content-md-center align-items-center">
                    <Col md='3'>
                        <Card>
                            <Card.Body className="justify-content-md-center mx-4 my-5 text-center">
                                <Card.Title className="mb-5">LOGIN</Card.Title>
                                <Form onSubmit={handleLogin}>
                                    <Form.Group className="mb-3" controlId="loginForm.email">                                       
                                        <Form.Control type="email" name="email" placeholder="Input Your Email" />
                                    </Form.Group>
                                    <Form.Group className="mb-3" controlId="loginForm.password">                                        
                                        <Form.Control type="password" name="password" placeholder="Input Your Password" />
                                    </Form.Group>
                                </Form>
                                <Button className="justify-content-md-center w-100" variant="warning">LOGIN</Button>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>                
        </Layout>
    );   
}

export default Login;