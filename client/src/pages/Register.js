import React from "react";
import { Layout } from '../layout';
import { Container, Row, Col, Card, Button, Form } from 'react-bootstrap';
import '../assets/css/auth.css';

const Register = () => {
    return (
        <Layout>            
            <Container fluid>
                <Row className="registerPage justify-content-md-center align-items-center">
                    <Col md='3'>
                        <Card>
                            <Card.Body className="justify-content-md-center mx-4 my-5 text-center">
                                <Card.Title className="mb-5">REGISTER</Card.Title>
                                <Form>
                                    <Form.Group className="mb-3" controlId="loginForm.email">                                       
                                        <Form.Control type="email" placeholder="Insert Your Email" />
                                    </Form.Group>
                                    <Form.Group className="mb-3" controlId="loginForm.password">                                        
                                        <Form.Control type="password" placeholder="Insert Your Password" />
                                    </Form.Group>
                                    <Form.Group className="mb-3" controlId="loginForm.password">                                        
                                        <Form.Control type="password" placeholder="Confirm Your Password" />
                                    </Form.Group>
                                </Form>
                                <Button className="justify-content-md-center w-100" variant="warning">REGISTER</Button>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>                
        </Layout>
    );
}

export default Register;