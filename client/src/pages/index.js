import Login from './Login';
import Register from './Register';
import Opponent from './Opponent';

export { Login, Register, Opponent };