import React, { useEffect, useState } from 'react';
import { Layout } from './layout';
import Opponent from './pages/Opponent';
import userData from './dummy-user.json';
import LoginForm from './pages/Login';

function App() {
  const [user, setUser] = useState({username: '', password: ''});
  const [error, setError] = useState('');

  // let loggedUser = userData.filter(user => user.username === userData)

  const Login = details => {
      console.log(details);
  }

  const Logout = () => {
      console.log('Logout');
  }


  return (
    <>
      {
        (user.username !== '' && user.password !== '') ? (            
          <Layout>
            <Opponent></Opponent>
          </Layout>                                        
        ):(            
          <LoginForm Login={Login} error={error} />           
        )
      }
    </>
  );
}

export default App;
