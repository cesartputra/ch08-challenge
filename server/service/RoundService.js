'use strict';


/**
 * Accept fight from other player
 * Endpoint for accept fight from other player
 *
 * body Object 
 * roomId String Id room
 * returns inline_response_201_8
 **/
exports.acceptFight = function(body,roomId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "createdAt" : "01-12-2021 01:01 +07:00",
    "winnerId" : "winnerId",
    "id" : "4bb280af-c637-44db-b359-fdb1500fac57",
    "userOption" : {
      "option" : { }
    },
    "roomId" : "2e79bc45-2ef5-4501-8ca8-a66da72f80a1",
    "updatedAt" : "01-12-2021 01:10 +07:00"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Create new round
 * Endpoint for create new round game
 *
 * body Object 
 * returns inline_response_201_8
 **/
exports.createRound = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "createdAt" : "01-12-2021 01:01 +07:00",
    "winnerId" : "winnerId",
    "id" : "4bb280af-c637-44db-b359-fdb1500fac57",
    "userOption" : {
      "option" : { }
    },
    "roomId" : "2e79bc45-2ef5-4501-8ca8-a66da72f80a1",
    "updatedAt" : "01-12-2021 01:10 +07:00"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Show round game info
 * Endpoint for showing all round game info
 *
 * returns inline_response_201_7
 **/
exports.getRound = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "createdAt" : "01-12-2021 01:01 +07:00",
    "winnerId" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
    "id" : "4bb280af-c637-44db-b359-fdb1500fac57",
    "userOption" : {
      "createdAt" : "01-12-2021 01:01 +07:00",
      "roundId" : "4bb280af-c637-44db-b359-fdb1500fac57",
      "userId" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
      "updatedAt" : "01-12-2021 01:10 +07:00",
      "option" : {
        "inferiorId" : 2,
        "createdAt" : "01-12-2021 01:01 +07:00",
        "name" : "ROCK",
        "id" : 1,
        "superiorId" : 3,
        "updatedAt" : "01-12-2021 01:10 +07:00"
      }
    },
    "roomId" : "2e79bc45-2ef5-4501-8ca8-a66da72f80a1",
    "updatedAt" : "01-12-2021 01:10 +07:00"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

