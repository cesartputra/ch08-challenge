'use strict';


/**
 * Register new user
 * Endpoint for register new user
 *
 * body Object 
 * returns inline_response_201_2
 **/
exports.userRegister = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "createdAt" : "20-12-2021 04:20 +07:00",
    "roleId" : 2,
    "biodata" : {
      "createdAt" : "20-12-2021 04:20 +07:00",
      "phoneNumber" : "81112125456",
      "address" : "Jl. Mangga",
      "avatarUrl" : "suitgames.com/digidaw.png",
      "fullName" : "Digidaw Aweu",
      "bio" : "yellow is evil, so don't drink your own pee",
      "updatedAt" : "20-12-2021 04:20 +07:00"
    },
    "username" : "digidaw",
    "updatedAt" : "20-12-2021 04:20 +07:00"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Show user's biodata
 * Endpoint for showing user's biodata
 *
 * returns inline_response_201_2
 **/
exports.v1ProfileBiodataGET = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "createdAt" : "20-12-2021 04:20 +07:00",
    "roleId" : 2,
    "biodata" : {
      "createdAt" : "20-12-2021 04:20 +07:00",
      "phoneNumber" : "81112125456",
      "address" : "Jl. Mangga",
      "avatarUrl" : "suitgames.com/digidaw.png",
      "fullName" : "Digidaw Aweu",
      "bio" : "yellow is evil, so don't drink your own pee",
      "updatedAt" : "20-12-2021 04:20 +07:00"
    },
    "username" : "digidaw",
    "updatedAt" : "20-12-2021 04:20 +07:00"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

