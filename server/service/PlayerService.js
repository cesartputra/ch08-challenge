'use strict';


/**
 * Accept fight from other player
 * Endpoint for accept fight from other player
 *
 * body Object 
 * roomId String Id room
 * returns inline_response_201_8
 **/
exports.acceptFight = function(body,roomId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "createdAt" : "01-12-2021 01:01 +07:00",
    "winnerId" : "winnerId",
    "id" : "4bb280af-c637-44db-b359-fdb1500fac57",
    "userOption" : {
      "option" : { }
    },
    "roomId" : "2e79bc45-2ef5-4501-8ca8-a66da72f80a1",
    "updatedAt" : "01-12-2021 01:10 +07:00"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Create new room
 * Endpoint to create new room
 *
 * body Object 
 * returns inline_response_201_4
 **/
exports.createRoom = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "roundCount" : 0,
    "roomStatus" : "OPEN",
    "createdAt" : "29-11-2021 00:01 +07:00",
    "winnerId" : "winnerId",
    "name" : "pedangdutasia",
    "roomOwner" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
    "id" : "2e79bc45-2ef5-4501-8ca8-a66da72f80a1",
    "userRoom" : {
      "createdAt" : "29-11-2021 00:01 +07:00",
      "userId" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
      "roomId" : "2e79bc45-2ef5-4501-8ca8-a66da72f80a1",
      "updatedAt" : "29-11-2021 00:01 +07:00"
    },
    "updatedAt" : "29-11-2021 00:01 +07:00"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Decline fight from other player
 * Endpoint for decline fight from other player
 *
 * roomId String Id room
 * returns inline_response_201_6
 **/
exports.declineFight = function(roomId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "message" : "Successfully delete data"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Show all players based on points or level
 * Endpoint for showing based on points or level
 *
 * body Object 
 * returns inline_response_201_19
 **/
exports.fightPlayer = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "roundCount" : 0,
    "roomStatus" : "CLOSED",
    "createdAt" : "29-11-2021 00:01 +07:00",
    "winnerId" : "winnerId",
    "name" : "metaverse",
    "roomOwner" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
    "id" : "5bca292b-007f-4435-b4a1-c8a7b02a52a9",
    "userRoom" : {
      "createdAt" : "29-11-2021 00:01 +07:00",
      "userId" : "3ceb530b-1ee9-4223-ac2d-4ca32a8f3f30",
      "roomId" : "5bca292b-007f-4435-b4a1-c8a7b02a52a9",
      "updatedAt" : "29-11-2021 00:01 +07:00"
    },
    "updatedAt" : "29-11-2021 00:01 +07:00"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Show game room info
 * Endpoint for showing game room info
 *
 * returns inline_response_201_3
 **/
exports.getRoom = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "roundCount" : 3,
    "roomStatus" : "CLOSED",
    "createdAt" : "29-11-2021 00:01 +07:00",
    "winnerId" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
    "name" : "pedangdutasia",
    "roomOwner" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
    "id" : "2e79bc45-2ef5-4501-8ca8-a66da72f80a1",
    "userRoom" : {
      "createdAt" : "29-11-2021 00:01 +07:00",
      "userId" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
      "roomId" : "2e79bc45-2ef5-4501-8ca8-a66da72f80a1",
      "updatedAt" : "29-11-2021 00:01 +07:00"
    },
    "updatedAt" : "29-11-2021 00:01 +07:00"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Show round game info
 * Endpoint for showing all round game info
 *
 * returns inline_response_201_7
 **/
exports.getRound = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "createdAt" : "01-12-2021 01:01 +07:00",
    "winnerId" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
    "id" : "4bb280af-c637-44db-b359-fdb1500fac57",
    "userOption" : {
      "createdAt" : "01-12-2021 01:01 +07:00",
      "roundId" : "4bb280af-c637-44db-b359-fdb1500fac57",
      "userId" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
      "updatedAt" : "01-12-2021 01:10 +07:00",
      "option" : {
        "inferiorId" : 2,
        "createdAt" : "01-12-2021 01:01 +07:00",
        "name" : "ROCK",
        "id" : 1,
        "superiorId" : 3,
        "updatedAt" : "01-12-2021 01:10 +07:00"
      }
    },
    "roomId" : "2e79bc45-2ef5-4501-8ca8-a66da72f80a1",
    "updatedAt" : "01-12-2021 01:10 +07:00"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Show all players based on points or level
 * Endpoint for showing based on points or level
 *
 * body Object 
 * returns inline_response_201_18
 **/
exports.searchPlayer = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "createdAt" : "20-12-2021 04:20 +07:00",
    "stats" : {
      "createdAt" : "01-12-2021 01:01 +07:00",
      "level" : "Class C",
      "winRate" : 80,
      "point" : 1500,
      "updatedAt" : "01-12-2021 01:10 +07:00"
    },
    "username" : "digidaw",
    "updatedAt" : "20-12-2021 04:20 +07:00"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Show user's biodata
 * Endpoint for showing user's biodata
 *
 * returns inline_response_201_2
 **/
exports.v1ProfileBiodataGET = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "createdAt" : "20-12-2021 04:20 +07:00",
    "roleId" : 2,
    "biodata" : {
      "createdAt" : "20-12-2021 04:20 +07:00",
      "phoneNumber" : "81112125456",
      "address" : "Jl. Mangga",
      "avatarUrl" : "suitgames.com/digidaw.png",
      "fullName" : "Digidaw Aweu",
      "bio" : "yellow is evil, so don't drink your own pee",
      "updatedAt" : "20-12-2021 04:20 +07:00"
    },
    "username" : "digidaw",
    "updatedAt" : "20-12-2021 04:20 +07:00"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Show user's biodata
 * Endpoint for showing user's biodata
 *
 * returns inline_response_201_9
 **/
exports.v1ProfileStatsGET = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "createdAt" : "01-12-2021 01:01 +07:00",
    "levelId" : 3,
    "id" : "7fa19f92-7b89-4c4c-9e1b-9e7ada157bbc",
    "winRate" : 80,
    "userId" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
    "point" : 1500,
    "updatedAt" : "01-12-2021 01:10 +07:00"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Join room game
 * Endpoint for join room game
 *
 * body Object 
 * returns inline_response_201_3
 **/
exports.v1RoomsJoinPOST = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "roundCount" : 3,
    "roomStatus" : "CLOSED",
    "createdAt" : "29-11-2021 00:01 +07:00",
    "winnerId" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
    "name" : "pedangdutasia",
    "roomOwner" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
    "id" : "2e79bc45-2ef5-4501-8ca8-a66da72f80a1",
    "userRoom" : {
      "createdAt" : "29-11-2021 00:01 +07:00",
      "userId" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
      "roomId" : "2e79bc45-2ef5-4501-8ca8-a66da72f80a1",
      "updatedAt" : "29-11-2021 00:01 +07:00"
    },
    "updatedAt" : "29-11-2021 00:01 +07:00"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Show all opened rooms
 * Endpoint for showing all opened rooms
 *
 * returns inline_response_201_16
 **/
exports.v1RoomsOpenGET = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "roundCount" : 0,
    "roomStatus" : "OPEN",
    "createdAt" : "29-11-2021 00:01 +07:00",
    "winnerId" : "winnerId",
    "name" : "gembokmuseum",
    "roomOwner" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
    "id" : "2e79bc45-2ef5-4501-8ca8-a66da72f80a1",
    "updatedAt" : "29-11-2021 00:01 +07:00"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Play game
 * Endpoint for play game
 *
 * body Object 
 * returns inline_response_201_7
 **/
exports.v1RoomsPlayPOST = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "createdAt" : "01-12-2021 01:01 +07:00",
    "winnerId" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
    "id" : "4bb280af-c637-44db-b359-fdb1500fac57",
    "userOption" : {
      "createdAt" : "01-12-2021 01:01 +07:00",
      "roundId" : "4bb280af-c637-44db-b359-fdb1500fac57",
      "userId" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
      "updatedAt" : "01-12-2021 01:10 +07:00",
      "option" : {
        "inferiorId" : 2,
        "createdAt" : "01-12-2021 01:01 +07:00",
        "name" : "ROCK",
        "id" : 1,
        "superiorId" : 3,
        "updatedAt" : "01-12-2021 01:10 +07:00"
      }
    },
    "roomId" : "2e79bc45-2ef5-4501-8ca8-a66da72f80a1",
    "updatedAt" : "01-12-2021 01:10 +07:00"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Show all played rooms
 * Endpoint for showing all played rooms
 *
 * returns inline_response_201_17
 **/
exports.v1RoomsPlayedGET = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "roundCount" : 3,
    "roomStatus" : "CLOSED",
    "createdAt" : "29-11-2021 00:01 +07:00",
    "winnerId" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
    "name" : "oliherbie",
    "roomOwner" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
    "id" : "2e79bc45-2ef5-4501-8ca8-a66da72f80a1",
    "updatedAt" : "29-11-2021 00:01 +07:00"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

