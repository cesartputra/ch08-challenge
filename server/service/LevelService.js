'use strict';


/**
 * Create a new level
 * This endpoint to create a new level
 *
 * body Object 
 * returns inline_response_201_12
 **/
exports.createLevel = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "minPoint" : 1800,
    "maxPoint" : 1999,
    "createdAt" : "29-11-2021 00:01 +07:00",
    "name" : "Class A",
    "id" : "e75bf4aa-2ae1-49a4-85b4-4cbbe73eeb72",
    "updatedAt" : "29-11-2021 00:01 +07:00"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Show all level
 * Endpoint for show all level
 *
 * returns inline_response_201_11
 **/
exports.getLevel = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "createdAt" : "01-12-2021 01:01 +07:00",
    "levelId" : 3,
    "id" : "7fa19f92-7b89-4c4c-9e1b-9e7ada157bbc",
    "winRate" : 80,
    "userId" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
    "point" : 1500,
    "updatedAt" : "01-12-2021 01:10 +07:00"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Edit Level
 * This endpoint to edit level
 *
 * body Object 
 * levelId String Id level
 * returns inline_response_201_13
 **/
exports.updateLevel = function(body,levelId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "minPoint" : 1850,
    "maxPoint" : 1900,
    "createdAt" : "29-11-2021 00:01 +07:00",
    "name" : "Class A",
    "id" : "e75bf4aa-2ae1-49a4-85b4-4cbbe73eeb72",
    "updatedAt" : "29-11-2021 00:01 +07:00"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Delete Level
 * This endpoint to delete level
 *
 * levelId String Id level
 * returns inline_response_201_6
 **/
exports.v1LevelLevelIdDELETE = function(levelId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "message" : "Successfully delete data"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

