'use strict';


/**
 * Show game room info
 * Endpoint for showing game room info
 *
 * returns inline_response_201_3
 **/
exports.getRoom = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "roundCount" : 3,
    "roomStatus" : "CLOSED",
    "createdAt" : "29-11-2021 00:01 +07:00",
    "winnerId" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
    "name" : "pedangdutasia",
    "roomOwner" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
    "id" : "2e79bc45-2ef5-4501-8ca8-a66da72f80a1",
    "userRoom" : {
      "createdAt" : "29-11-2021 00:01 +07:00",
      "userId" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
      "roomId" : "2e79bc45-2ef5-4501-8ca8-a66da72f80a1",
      "updatedAt" : "29-11-2021 00:01 +07:00"
    },
    "updatedAt" : "29-11-2021 00:01 +07:00"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Show round game info
 * Endpoint for showing all round game info
 *
 * returns inline_response_201_7
 **/
exports.getRound = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "createdAt" : "01-12-2021 01:01 +07:00",
    "winnerId" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
    "id" : "4bb280af-c637-44db-b359-fdb1500fac57",
    "userOption" : {
      "createdAt" : "01-12-2021 01:01 +07:00",
      "roundId" : "4bb280af-c637-44db-b359-fdb1500fac57",
      "userId" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
      "updatedAt" : "01-12-2021 01:10 +07:00",
      "option" : {
        "inferiorId" : 2,
        "createdAt" : "01-12-2021 01:01 +07:00",
        "name" : "ROCK",
        "id" : 1,
        "superiorId" : 3,
        "updatedAt" : "01-12-2021 01:10 +07:00"
      }
    },
    "roomId" : "2e79bc45-2ef5-4501-8ca8-a66da72f80a1",
    "updatedAt" : "01-12-2021 01:10 +07:00"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Show all users
 * Endpoint for showing all users
 *
 * returns inline_response_201_20
 **/
exports.v1UsersGET = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "user" : {
    "createdAt" : "20-12-2021 04:20 +07:00",
    "roleId" : 2,
    "biodata" : {
      "createdAt" : "20-12-2021 04:20 +07:00",
      "phoneNumber" : "81112125456",
      "address" : "Jl. Mangga",
      "avatarUrl" : "suitgames.com/digidaw.png",
      "fullName" : "Digidaw Aweu",
      "bio" : "yellow is evil, so don't drink your own pee",
      "id" : "f4b5292d-f767-4436-8951-7f04dcad29ce",
      "userId" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
      "updatedAt" : "20-12-2021 04:20 +07:00"
    },
    "id" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
    "username" : "digidaw",
    "updatedAt" : "20-12-2021 04:20 +07:00"
  }
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

