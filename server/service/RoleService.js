'use strict';


/**
 * Create new role
 * This endpoint for create a new role
 *
 * body Object 
 * returns inline_response_201_15
 **/
exports.createRole = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "createdAt" : "26-12-2021 12:01 +07:00",
    "name" : "ADMIN",
    "id" : 1,
    "updatedAt" : "26-12-2021 12:01 +07:00"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Retrieve roles user
 * This endpoint to retrieve all user role are available
 *
 * returns inline_response_201_14
 **/
exports.getRole = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "user" : {
    "createdAt" : "20-12-2021 04:20 +07:00",
    "name" : "Admin",
    "id" : 1,
    "updatedAt" : "20-12-2021 04:20 +07:00"
  }
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Update role
 * Endpoint for update role
 *
 * body Object 
 * roleId String Id role
 * returns inline_response_201_15
 **/
exports.updateRole = function(body,roleId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "createdAt" : "26-12-2021 12:01 +07:00",
    "name" : "ADMIN",
    "id" : 1,
    "updatedAt" : "26-12-2021 12:01 +07:00"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Delete Role
 * Rndpoint for delete role
 *
 * roleId String Id role
 * returns inline_response_201_6
 **/
exports.v1RolesRoleIdDELETE = function(roleId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "message" : "Successfully delete data"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

