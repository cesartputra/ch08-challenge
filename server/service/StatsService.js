'use strict';


/**
 * Create statistic
 * This endpoint to create statistic player
 *
 * body Object 
 * returns inline_response_201_10
 **/
exports.createStats = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "createdAt" : "01-12-2021 01:01 +07:00",
    "levelId" : 0,
    "id" : "7fa19f92-7b89-4c4c-9e1b-9e7ada157bbc",
    "winRate" : 0,
    "userId" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
    "point" : 0,
    "updatedAt" : "01-12-2021 01:10 +07:00"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Showing user's stats.
 * Endpoint for showing all user's stats
 *
 * returns inline_response_201_9
 **/
exports.getStats = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "createdAt" : "01-12-2021 01:01 +07:00",
    "levelId" : 3,
    "id" : "7fa19f92-7b89-4c4c-9e1b-9e7ada157bbc",
    "winRate" : 80,
    "userId" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
    "point" : 1500,
    "updatedAt" : "01-12-2021 01:10 +07:00"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Show user's biodata
 * Endpoint for showing user's biodata
 *
 * returns inline_response_201_9
 **/
exports.v1ProfileStatsGET = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "createdAt" : "01-12-2021 01:01 +07:00",
    "levelId" : 3,
    "id" : "7fa19f92-7b89-4c4c-9e1b-9e7ada157bbc",
    "winRate" : 80,
    "userId" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
    "point" : 1500,
    "updatedAt" : "01-12-2021 01:10 +07:00"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

