'use strict';


/**
 * Show all players based on points or level
 * Endpoint for showing based on points or level
 *
 * body Object 
 * returns inline_response_201_19
 **/
exports.fightPlayer = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "roundCount" : 0,
    "roomStatus" : "CLOSED",
    "createdAt" : "29-11-2021 00:01 +07:00",
    "winnerId" : "winnerId",
    "name" : "metaverse",
    "roomOwner" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
    "id" : "5bca292b-007f-4435-b4a1-c8a7b02a52a9",
    "userRoom" : {
      "createdAt" : "29-11-2021 00:01 +07:00",
      "userId" : "3ceb530b-1ee9-4223-ac2d-4ca32a8f3f30",
      "roomId" : "5bca292b-007f-4435-b4a1-c8a7b02a52a9",
      "updatedAt" : "29-11-2021 00:01 +07:00"
    },
    "updatedAt" : "29-11-2021 00:01 +07:00"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Show all players based on points or level
 * Endpoint for showing based on points or level
 *
 * body Object 
 * returns inline_response_201_18
 **/
exports.searchPlayer = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "createdAt" : "20-12-2021 04:20 +07:00",
    "stats" : {
      "createdAt" : "01-12-2021 01:01 +07:00",
      "level" : "Class C",
      "winRate" : 80,
      "point" : 1500,
      "updatedAt" : "01-12-2021 01:10 +07:00"
    },
    "username" : "digidaw",
    "updatedAt" : "20-12-2021 04:20 +07:00"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Generate access token for user
 * Endpoint for login using correct data to generate access token
 *
 * body Object 
 * returns inline_response_201_1
 **/
exports.userLogin = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "message" : "Successfully login!",
    "accessToken" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Register new user
 * Endpoint for register new user
 *
 * body Object 
 * returns inline_response_201_2
 **/
exports.userRegister = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "createdAt" : "20-12-2021 04:20 +07:00",
    "roleId" : 2,
    "biodata" : {
      "createdAt" : "20-12-2021 04:20 +07:00",
      "phoneNumber" : "81112125456",
      "address" : "Jl. Mangga",
      "avatarUrl" : "suitgames.com/digidaw.png",
      "fullName" : "Digidaw Aweu",
      "bio" : "yellow is evil, so don't drink your own pee",
      "updatedAt" : "20-12-2021 04:20 +07:00"
    },
    "username" : "digidaw",
    "updatedAt" : "20-12-2021 04:20 +07:00"
  },
  "status" : "SUCCESS"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Show all users
 * Endpoint for showing all users
 *
 * returns inline_response_201_20
 **/
exports.v1UsersGET = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "user" : {
    "createdAt" : "20-12-2021 04:20 +07:00",
    "roleId" : 2,
    "biodata" : {
      "createdAt" : "20-12-2021 04:20 +07:00",
      "phoneNumber" : "81112125456",
      "address" : "Jl. Mangga",
      "avatarUrl" : "suitgames.com/digidaw.png",
      "fullName" : "Digidaw Aweu",
      "bio" : "yellow is evil, so don't drink your own pee",
      "id" : "f4b5292d-f767-4436-8951-7f04dcad29ce",
      "userId" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
      "updatedAt" : "20-12-2021 04:20 +07:00"
    },
    "id" : "5b789cb6-49fb-4ac6-ab7d-f1f0f9450088",
    "username" : "digidaw",
    "updatedAt" : "20-12-2021 04:20 +07:00"
  }
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

