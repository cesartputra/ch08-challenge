'use strict';

var utils = require('../utils/writer.js');
var Room = require('../service/RoomService');

module.exports.createRoom = function createRoom (req, res, next, body) {
  Room.createRoom(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.declineFight = function declineFight (req, res, next, roomId) {
  Room.declineFight(roomId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.deleteRoom = function deleteRoom (req, res, next, roomId) {
  Room.deleteRoom(roomId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getRoom = function getRoom (req, res, next) {
  Room.getRoom()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.updateRoom = function updateRoom (req, res, next, body, roomId) {
  Room.updateRoom(body, roomId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1RoomsJoinPOST = function v1RoomsJoinPOST (req, res, next, body) {
  Room.v1RoomsJoinPOST(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1RoomsOpenGET = function v1RoomsOpenGET (req, res, next) {
  Room.v1RoomsOpenGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1RoomsPlayPOST = function v1RoomsPlayPOST (req, res, next, body) {
  Room.v1RoomsPlayPOST(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1RoomsPlayedGET = function v1RoomsPlayedGET (req, res, next) {
  Room.v1RoomsPlayedGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
