'use strict';

var utils = require('../utils/writer.js');
var Level = require('../service/LevelService');

module.exports.createLevel = function createLevel (req, res, next, body) {
  Level.createLevel(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getLevel = function getLevel (req, res, next) {
  Level.getLevel()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.updateLevel = function updateLevel (req, res, next, body, levelId) {
  Level.updateLevel(body, levelId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1LevelLevelIdDELETE = function v1LevelLevelIdDELETE (req, res, next, levelId) {
  Level.v1LevelLevelIdDELETE(levelId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
