'use strict';

var utils = require('../utils/writer.js');
var Admin = require('../service/AdminService');

module.exports.getRoom = function getRoom (req, res, next) {
  Admin.getRoom()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getRound = function getRound (req, res, next) {
  Admin.getRound()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1UsersGET = function v1UsersGET (req, res, next) {
  Admin.v1UsersGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
