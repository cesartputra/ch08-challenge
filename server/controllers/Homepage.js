'use strict';

var utils = require('../utils/writer.js');
var Homepage = require('../service/HomepageService');

module.exports.rootGET = function rootGET (req, res, next) {
  Homepage.rootGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
