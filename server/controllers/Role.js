'use strict';

var utils = require('../utils/writer.js');
var Role = require('../service/RoleService');

module.exports.createRole = function createRole (req, res, next, body) {
  Role.createRole(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getRole = function getRole (req, res, next) {
  Role.getRole()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.updateRole = function updateRole (req, res, next, body, roleId) {
  Role.updateRole(body, roleId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1RolesRoleIdDELETE = function v1RolesRoleIdDELETE (req, res, next, roleId) {
  Role.v1RolesRoleIdDELETE(roleId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
