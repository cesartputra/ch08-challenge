'use strict';

var utils = require('../utils/writer.js');
var Player = require('../service/PlayerService');

module.exports.acceptFight = function acceptFight (req, res, next, body, roomId) {
  Player.acceptFight(body, roomId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.createRoom = function createRoom (req, res, next, body) {
  Player.createRoom(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.declineFight = function declineFight (req, res, next, roomId) {
  Player.declineFight(roomId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.fightPlayer = function fightPlayer (req, res, next, body) {
  Player.fightPlayer(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getRoom = function getRoom (req, res, next) {
  Player.getRoom()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getRound = function getRound (req, res, next) {
  Player.getRound()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.searchPlayer = function searchPlayer (req, res, next, body) {
  Player.searchPlayer(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1ProfileBiodataGET = function v1ProfileBiodataGET (req, res, next) {
  Player.v1ProfileBiodataGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1ProfileStatsGET = function v1ProfileStatsGET (req, res, next) {
  Player.v1ProfileStatsGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1RoomsJoinPOST = function v1RoomsJoinPOST (req, res, next, body) {
  Player.v1RoomsJoinPOST(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1RoomsOpenGET = function v1RoomsOpenGET (req, res, next) {
  Player.v1RoomsOpenGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1RoomsPlayPOST = function v1RoomsPlayPOST (req, res, next, body) {
  Player.v1RoomsPlayPOST(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1RoomsPlayedGET = function v1RoomsPlayedGET (req, res, next) {
  Player.v1RoomsPlayedGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
