'use strict';

var utils = require('../utils/writer.js');
var Biodata = require('../service/BiodataService');

module.exports.userRegister = function userRegister (req, res, next, body) {
  Biodata.userRegister(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1ProfileBiodataGET = function v1ProfileBiodataGET (req, res, next) {
  Biodata.v1ProfileBiodataGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
